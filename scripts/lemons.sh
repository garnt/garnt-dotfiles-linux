# Filename:	lemons.py
# Author:	Garnt, 2016
# Desc:		A script to generate information to be displayed in lemonbar

clock() {
	# Get the date
	date '+%B %d, %Y'
}

volume() {
	if $(pamixer --get-mute); then
		volsym=$(echo -e "\uf026")"  "
	else
		volsym=$(echo -e "\uf028")"  "
	fi
	echo $volsym$(pamixer --get-volume)
}

nowplaying() {
	cur=`mpc current`
    # this line allow to choose whether the output will scroll or not
    test "$1" = "scroll" && PARSER='skroll -n20 -d0.5 -r' || PARSER='cat'
    test -n "$cur" && $PARSER <<< $cur || echo "- stopped -"
}

while :; do
	buf="${buf}%{l}%{F#4c4c4c} Today is: %{F#ffffff}$(clock)"
	buf="${buf}%{c}%{F#4c4c4c} Now Playing: %{F#ffffff}$(nowplaying)"
	buf="${buf}%{r}%{F#ffffff}$(volume)"

	echo $buf
	# use `nowplaying scroll` to get a scrolling output!
	sleep 0.5 # The HUD will be updated every second
done
