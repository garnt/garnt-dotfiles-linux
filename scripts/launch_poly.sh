#!/usr/bin/env sh

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -x polybar >/dev/null; do sleep 1; done

# Launch bar1 and bar2
MONITOR=DVI-0 polybar top &
MONITOR=DVI-0 polybar bottom &
MONITOR=DVI-1 polybar top &
MONITOR=DVI-1 polybar bottom &

echo "Bars launched..."
