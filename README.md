# Dotfiles
### Install Instructions:
Copy all files to the '.config' folder in your home directory.

### Required Applications/Libraries:
- vim (editor)
- i3-gaps (wm)
- i3-lock-fancy (screen lock)
- feh (background)
- dunst (notifications)
- polybar (info bars)
- FontAwesome (icon font)

### Optional Applications/Libraries
- Mopidy (mpd server)
