; [settings]
; throttle-ms = 50
; throttle-limit = 3

[bar/top]
monitor = ${env:MONITOR:DVI-1}
width = 100%
height = 34
clickareas = 35

background = #00000000
foreground = #ccffffff
linecolor = ${bar/bottom.background}

spacing = 4
;spacing = 2
lineheight = 16
padding-right = 5
module-margin-left = 3
module-margin-right = 3
;module-margin-left = 1
;module-margin-right = 1

;font-0 = envypn:pixelsize=13;0
font-0 = NotoSans-Regular:size=8;0
font-1 = MaterialIcons:size=10;0
font-2 = Termsynu:size=8;-1
font-3 = FontAwesome:size=10;0

modules-left = powermenu mpd
modules-center = i3
modules-right = date

[bar/bottom]
monitor = ${env:MONITOR:DVI-1}
bottom = true
width = 100%
height = 27

background = ${bar/top.background}
foreground = ${bar/top.foreground}
linecolor = ${bar/top.background}

spacing = 3
lineheight = 2
padding-right = 4
module-margin-left = 0
module-margin-right = 6

font-0 = NotoSans-Regular:size=8;0
font-1 = Unifont:size=6;-3
font-2 = FontAwesome:size=8;-2
font-3 = NotoSans-Regular:size=8;-1
font-4 = MaterialIcons:size=10;-1
font-5 = Termsynu:size=8;-1

modules-left =
modules-right = wireless-network wired-network-1 wired-network-2 cpu memory

[bar/external_bottom]
;inherit-from = bar/bottom

monitor = HDMI-1
bottom = true
width = ${bar/bottom.width}
height = ${bar/bottom.height}

background = ${bar/bottom.background}
foreground = ${bar/bottom.foreground}
linecolor = ${bar/bottom.linecolor}

spacing = ${bar/bottom.spacing}
lineheight = ${bar/bottom.lineheight}
padding-right = ${bar/bottom.padding_right}
module-margin-left = ${bar/bottom.module_margin_left}
module-margin-right = ${bar/bottom.module_margin_right}

font-0 = ${bar/bottom.font-0}
font-1 = ${bar/bottom.font-1}
font-2 = ${bar/bottom.font-2}
font-3 = ${bar/bottom.font-3}
font-4 = ${bar/bottom.font-4}

modules-left = bspwm
modules-right = clock

[module/bspwm]
type = internal/bspwm

label-focused = %index%
label-focused-background = ${colors.background-alt}
label-focused-underline= ${colors.primary}
label-focused-padding = 2

label-occupied = %index%
label-occupied-padding = 2

label-urgent = %index%!
label-urgent-background = ${colors.alert}
label-urgent-padding = 2

label-empty = %index%
label-empty-foreground = ${colors.foreground-alt}

[module/i3]
type = internal/i3
format = <label-state> <label-mode>
index-sort = true
wrapping-scroll = true
strip-wsnumbers = true
pin-workspaces = true

label-mode-padding = 5
label-mode-foreground = #ffffff

label-unfocused-foreground = #555555
label-unfocused-padding = 1

label-visible-foregroud = #888888
label-visible-padding = 1

label-focused-foreground = #ffffff
label-focused-padding = 1

label-urgent-padding = 1

; urgent = Workspace with urgency hint set
label-urgent = %index%
label-urgent-background = #{bar/top.background}

[module/cpu]
type = internal/cpu
interval = 0.5
format = <label> <ramp-coreload>
label = %%{F#dd}CPU

ramp-coreload-0 = ▁
ramp-coreload-0-font = 2
ramp-coreload-0-foreground = #aaff77
ramp-coreload-1 = ▂
ramp-coreload-1-font = 2
ramp-coreload-1-foreground = #aaff77
ramp-coreload-2 = ▃
ramp-coreload-2-font = 2
ramp-coreload-2-foreground = #aaff77
ramp-coreload-3 = ▄
ramp-coreload-3-font = 2
ramp-coreload-3-foreground = #aaff77
ramp-coreload-4 = ▅
ramp-coreload-4-font = 2
ramp-coreload-4-foreground = #fba922
ramp-coreload-5 = ▆
ramp-coreload-5-font = 2
ramp-coreload-5-foreground = #fba922
ramp-coreload-6 = ▇
ramp-coreload-6-font = 2
ramp-coreload-6-foreground = #ff5555
ramp-coreload-7 = █
ramp-coreload-7-font = 2
ramp-coreload-7-foreground = #ff5555

[module/date]
type = internal/date
date =    %%{F#99}%Y-%m-%d%%{F-}  %%{F#fff}%H:%M%%{F-}
date-alt = %%{F#fff}%A, %d %B %Y  %%{F#fff}%H:%M%%{F#666}:%%{F#fba922}%S%%{F-}

[module/memory]
type = internal/memory
format = <label> <bar-used>
label = %%{F#dd}RAM

bar-used-width = 30
bar-used-foreground-0 = #aaff77
bar-used-foreground-1 = #aaff77
bar-used-foreground-2 = #fba922
bar-used-foreground-3 = #ff5555
bar-used-indicator = |
bar-used-indicator-font = 6
bar-used-indicator-foreground = #ff
bar-used-fill = ─
bar-used-fill-font = 6
bar-used-empty = ─
bar-used-empty-font = 6
bar-used-empty-foreground = #444444

[module/mpd]
type = internal/mpd
format-online = <icon-prev> <icon-stop> <toggle> <icon-next>  <icon-repeat> <icon-random>  <bar-progress> <label-time>  <label-song>

icon-play = 
icon-pause = 
icon-stop = 
icon-prev = 
icon-next = 
icon-random = 
icon-repeat = 

toggle-on-foreground =
toggle-off-foreground = #55

bar-progress-width = 45
bar-progress-format = %{A4:mpdseek+2: A5:mpdseek-2:}%fill%%indicator%%empty%%{A A}
bar-progress-indicator = |
bar-progress-indicator-foreground = #ff
bar-progress-indicator-font = 3
bar-progress-fill = ─
bar-progress-fill-foreground = #bb
bar-progress-fill-font = 3
bar-progress-empty = ─
bar-progress-empty-font = 3
bar-progress-empty-foreground = #44

[module/wireless-network]
type = internal/network
interface = wlp2s0
interval = 3.0
ping-interval = 10

format-connected = <ramp-signal> <label-connected>
label-connected = %essid%
label-disconnected =    not connected
label-disconnected-foreground = #dd

ramp-signal-0 = 
ramp-signal-1 = 
ramp-signal-2 = 
ramp-signal-3 = 
ramp-signal-4 = 

animation-packetloss-0 = 
animation-packetloss-0-foreground = #ffa64c
animation-packetloss-1 = 
animation-packetloss-1-foreground = ${bar/top.foreground}
animation-packetloss-framerate = 500

[module/wired-network-1]
type = internal/network
interface = eno1
interval = 3.0

label-connected =    %{T3}%local_ip%%{T-}
label-disconnected-foreground = #dd

[module/wired-network-2]
type = internal/network
interface = enp5s0
interval = 3.0

label-connected =    %{T3}%local_ip%%{T-}
label-disconnected-foreground = #dd

[module/volume]
type = internal/volume
speaker-mixer = Speaker
headphone-mixer = Headphone
headphone-id = 9

format-volume = <ramp-volume> <label-volume>
label-muted =   muted
label-muted-foreground = #dd

ramp-volume-0 = 
ramp-volume-1 = 
ramp-volume-2 = 
ramp-volume-3 = 

[module/powermenu]
type = custom/menu

format-padding = 5

label-open = 
label-close = 

menu-0-0 = Terminate WM
menu-0-0-foreground = #fba922
menu-0-0-exec = bspc quit -1
menu-0-1 = Reboot
menu-0-1-foreground = #fba922
menu-0-1-exec = menu_open-1
menu-0-2 = Power off
menu-0-2-foreground = #fba922
menu-0-2-exec = menu_open-2

menu-1-0 = Cancel
menu-1-0-foreground = #fba922
menu-1-0-exec = menu_open-0
menu-1-1 = Reboot
menu-1-1-foreground = #fba922
menu-1-1-exec = sudo reboot

menu-2-0 = Power off
menu-2-0-foreground = #fba922
menu-2-0-exec = sudo poweroff
menu-2-1 = Cancel
menu-2-1-foreground = #fba922
menu-2-1-exec = menu_open-0

[module/clock]
type = internal/date
interval = 2
date = %%{F#ddd}%Y-%m-%d%%{F-}  %%{F#fff}%H:%M%%{F-}

; vim:ft=dosini
